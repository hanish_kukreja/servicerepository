//
//  ServiceDemo.swift
//  ServiceDemo_Swift
//
//  Created by Hanish on 06/05/16.
//  Copyright © 2016 Hanish. All rights reserved.
//

import UIKit

protocol ServiceDemoDelegate : class{
    func didFinishTask (sender :ServiceDemo , data : String)

    func showAlert(message :String)
}

class ServiceDemo: NSObject ,NSURLSessionDownloadDelegate {
    
    var dataString :String?
    var delegate : ServiceDemoDelegate?
    
    
    func getJSONData(url :String ,keyword :String)->Void
    {
        
        let defaultSession = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        
        var dataTask: NSURLSessionDataTask?
        let baseURL :NSURLComponents! = NSURLComponents(string:url)
        print(baseURL.host)
        print(baseURL.path)
        var query :NSURLQueryItem = NSURLQueryItem(name: "text", value: keyword)
        baseURL.queryItems = [query]
        print(baseURL.queryItems)
        let urlNew :NSURL
        urlNew = baseURL.URL!
        
        
        dataTask = defaultSession.dataTaskWithURL(urlNew, completionHandler:{(data,response,error) in
            if let error = error{
                print(error.localizedDescription)
                
            }
            else if let httpResponse = response as? NSHTTPURLResponse{
                if httpResponse.statusCode == 200 {
                    self.dataString = NSString(data: data!, encoding: NSUTF8StringEncoding) as! String
                    print("task accomplished!!")
                    self.delegate?.didFinishTask(self, data: self.dataString!)
                }
            }
            
        } )
        
        dataTask?.resume()
        
        
    }
    
    
    func postJSONData()->Void{
        
        let url = NSURL(string:"http://petstore.swagger.io/v2/user")
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        let params:[String : String] =
        [
            "id"        :"106",
            "username"  :"hah",
            "firstName" :"Harry",
            "lastName"  :"Geller",
            "email"     :"h@gmail.com",
            "password"  :"harry",
            "phone"     :"9876543210",
            "userStatus":"1"
        ]
        print(params)
        let request = NSMutableURLRequest(URL: url!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let inputJSON : NSData = NSKeyedArchiver.archivedDataWithRootObject(params)
        
        do{
            request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(params, options: [])
        }
        catch{
            print(error)
        }
        
        
        let task = session.dataTaskWithRequest(request, completionHandler:{(data,response,error) in
            if let httpResponse = response as? NSHTTPURLResponse{
                
                print(response?.description)
                
                if httpResponse.statusCode != 200{
                    print("response was not 200")
                    return
                }
                if (error != nil){
                    print("error submitting the data")
                    return
                }
                do{
                    guard let responsedata = data else{
                        return
                    }
                    
                    
                    let result = try NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions(rawValue: 0))
                    print(result)
                    
                    
                }
                catch{
                    print(error)
                }
                
            }
        })
        
        task.resume()
    }
    
    func downloadFile(url :String)->Void{
        //your code here
        let urlNew =  NSURL(string: url)
        let downloadSession : NSURLSession = {
            let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
            let session = NSURLSession(configuration: configuration, delegate: self, delegateQueue: nil)
            return session
        }()
        
        
        let download = Download(url: url)
        download.downloadTask = downloadSession.downloadTaskWithURL(urlNew!)
        download.downloadTask!.resume()
        download.isDownloading = true
        
        
        
    }
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didFinishDownloadingToURL location: NSURL) {
         let originalURL = downloadTask.originalRequest?.URL?.absoluteString,
            destinationURL = localFilePathForUrl(originalURL!)
        print(destinationURL)
        
        let fileURL = NSURL(fileURLWithPath: destinationURL)
        print(fileURL)
        let fileManager = NSFileManager.defaultManager()
        
        
        
        
//        do{
//            try fileManager.removeItemAtPath(destinationURL)
//        }
//        catch{
//            print("error occured while deleting the file")
//        }
        if fileManager.fileExistsAtPath(destinationURL){
            self.delegate?.showAlert("File already exists.")
            
        }
        else{
        
        
        do{
            try fileManager.copyItemAtURL(location, toURL: fileURL)
        }
        catch let error as NSError{
            print("could not copy \(error.localizedDescription)")
            
        }
   }
        
       
       
        print("Finished downloading.")
        SVProgressHUD.dismiss()
    }
    
   
    
    func localFilePathForUrl(url :String)->String
    {
        var destination = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
        let fileUrl = NSURL(string: url)

        destination += "/\(fileUrl!.lastPathComponent!)"
        print(destination)
     
        return destination
    }
    
}



class Download :NSObject{
    var url : String
    var isDownloading = false
    var progress :Float = 0.0
    var downloadTask :NSURLSessionDownloadTask?
    var resumeData :NSData?
    
    init(url :String) {
        self.url = url
    }
    
}


