//
//  ViewController.swift
//  ServiceDemo_Swift
//
//  Created by Hanish on 06/05/16.
//  Copyright © 2016 Hanish. All rights reserved.
//

import UIKit
import QuartzCore

class ViewController: UIViewController {
    
    @IBOutlet weak var keywordText: UITextField!
    @IBOutlet weak var urlTextField: UITextField!
    @IBAction func getDataBtn(sender: AnyObject) {
        
        var serviceObject :ServiceDemo
        serviceObject = ServiceDemo()
        serviceObject.delegate = self
        
        SVProgressHUD.show()
        serviceObject.getJSONData(urlTextField.text!, keyword: keywordText.text!)
        

        
        
    }
    @IBAction func postDataBtn(sender: AnyObject) {
        var postObject :ServiceDemo
        postObject = ServiceDemo()
        postObject.postJSONData()
        
        
        
        
    }
    
    @IBAction func downloadFileBtn(sender: AnyObject) {
        var downloadObject :ServiceDemo
        downloadObject = ServiceDemo()
        downloadObject.delegate = self
        downloadObject.downloadFile("http://ggnindia.dronacharya.info/ecedept/Downloads/LecturePlan/AUG09_DEC09/III_Sem/ECONOMICS_III-Sem.html")
        //http://www.gbpec.edu.in/placement/doc/Placement-in-the-year-2014-15.docx
        
        //http://www.queness.com/resources/images/png/apple_ex.png
        SVProgressHUD.show()
        

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // 1
                
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
extension ViewController : ServiceDemoDelegate{
    func didFinishTask(sender: ServiceDemo , data :String) {
        print("delegate hit")
        SVProgressHUD.dismiss()
        print(data)
        
    }

    func showAlert(message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        

    }
    
       
}

